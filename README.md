# MagMessenger

MagMessenger it's a free, open source messenger  

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Before install the server or client (or both) side of the messenger you need to install Python (https://www.python.org/) version 3.6 or latest version on your machine and pip (https://pip.pypa.io/en/stable/) (the package installer for Python). Pip is usually install on machine along when you install Python in your system.

### Installing

1. Download MagMessenger's repository to your machine
2. Create a Python virtual environment
3. Go to MagMessenger's directory
4. Install requirements from requirements.txt

The installing will be done. At first launch a server and then launch a client. The client will find the server automatically. After the client will be connected to the server, you can see a new connection in the server's log-file and the client's window will show a greeting prompt for you.

## Running the tests

Run run-tests.py script to start automated tests. Logs are stored in 'log' sub directory.

## Deployment

If you deploy the messenger on a live system, then you should care of a firewall settings. Also highly recommend to use any load balancer in front of server.

## Built With

* [PyQt](https://riverbankcomputing.com/software/pyqt/intro) - Used to make a cross-platform GUI
* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/mag-messenger/codeconduct) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the https://github.com/mag-messenger/tags

## Authors

* **Pavel** - *Initial work* - [Pavel](https://github.com/Pavel)

See also the list of [contributors](https://github.com/mag-messenger/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Let's make the world better
